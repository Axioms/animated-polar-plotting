/**
 * Visually demonstrating the plotting of several types of polar curves.
 */

#include <array>
#include <cmath>
#include <string>
#include <sstream>
#include <iostream>
#include <SFML/Graphics.hpp>

const int SCREEN_WIDTH = 1400;
const int SCREEN_HEIGHT = 1400;
const sf::Vector2i ORIGIN(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);

//Speed at which the coefficients "a" and "b" are changed
const float SPEED = 1.25;
//Number of lines segments to draw each curve with
const int QUALITY = 1000;
//How much the graph is scaled
const float SCALE = 100;

const int LIMACON = 0;
const int CARDIOD = 1;
const int ROSE_CURVE = 2;
const int LEMNISCATE = 3;
const std::string NAMES[] = { "Limacon", "Cardiod", "Rose Curve", "Lemniscate" };
const int SIN = 0;
const int COS = 1;

std::array<bool, sf::Keyboard::KeyCount> keyStates;

bool isKeyPressed(sf::Keyboard::Key key) {
    return keyStates[static_cast<int>(key)];
}

struct PolarCurve: public sf::Drawable {
    float a;
    float b;
    float thetaStep;
    int segments;
    int type;
    sf::VertexArray vertices;

    PolarCurve(int numSegments, int numCycles, sf::Color color) :
            a(1),
            b(1),
            segments(numSegments),
            type(SIN),
            vertices(sf::PrimitiveType::Lines, numSegments * 2) {
        updateNumCycles(numCycles);
        for (int i = 0; i < vertices.getVertexCount(); i++) {
            vertices[i] = sf::Vertex(sf::Vector2f(0, 0), color);
        }
    }

    void draw(sf::RenderTarget &target, sf::RenderStates states) const {
        target.draw(vertices);
    }

    virtual ~PolarCurve() {
    }

    void updateNumCycles(float numCycles) {
        thetaStep = numCycles * 2 * M_PI / segments;
    }

    void update() {
        //Update start point individually
        float r = calcR(0);
        updateVertex(vertices[0], r, 0);

        //Update pair points
        for (int i = 1; i < vertices.getVertexCount(); i += 2) {
            float theta = thetaStep * i;
            float r = calcR(theta);
            updateVertex(vertices[i], r, theta);
            updateVertex(vertices[i + 1], r, theta);
        }
    }

    virtual float calcR(float theta) =0;

    //Switch sin/cos
    void switchType() {
        type = 1 - type;
    }

    //Give equation as a string
    virtual std::string toString() =0;

    //Convert from polar coordinates to cartesian
    void updateVertex(sf::Vertex &v, float r, float t) {
        v.position.x = ORIGIN.x + r * std::cos(t) * SCALE;
        v.position.y = ORIGIN.y + r * std::sin(t) * SCALE;
    }
};

struct Limacon: public PolarCurve {
    using PolarCurve::PolarCurve;

    float calcR(float theta) {
        if (type == SIN) {
            return a + b * std::sin(theta);
        } else {
            return a + b * std::cos(theta);
        }
    }

    std::string toString() {
        std::stringstream ss;
        ss << "r = (" << a << ") + (" << b << ") ";
        if (type == SIN) {
            ss << "sin";
        } else {
            ss << "cos";
        }
        ss << "(theta)";

        return ss.str();
    }
};

struct Cardiod: public PolarCurve {
    using PolarCurve::PolarCurve;
    float c = a;

    float calcR(float theta) {
        if (a != c) {
            b = a;
            c = a;
        } else if (b != c) {
            a = b;
            c = b;
        }

        if (type == SIN) {
            return a + b * std::sin(theta);
        } else {
            return a + b * std::cos(theta);
        }
    }

    std::string toString() {
        std::stringstream ss;
        ss << "r = (" << c << ") + (" << c << ") ";
        if (type == SIN) {
            ss << "sin";
        } else {
            ss << "cos";
        }
        ss << "(theta)";

        return ss.str();
    }
};

struct RoseCurve: public PolarCurve {
    using PolarCurve::PolarCurve;

    float calcR(float theta) {
        if (type == SIN) {
            return a * std::sin(b * theta);
        } else {
            return a * std::cos(b * theta);
        }
    }

    std::string toString() {
        std::stringstream ss;
        ss << "r = (" << a << ") ";
        if (type == SIN) {
            ss << "sin(";
        } else {
            ss << "cos(";
        }
        ss << b << " * " << "theta)";

        return ss.str();
    }
};

struct Lemniscate: public PolarCurve {
    using PolarCurve::PolarCurve;
    float c = a;

    float calcR(float theta) {
        if (a != c) {
            b = a;
            c = a;
        } else if (b != c) {
            a = b;
            c = b;
        }

        if (type == SIN) {
            return std::sqrt(a * a * std::sin(2 * theta));
        } else {
            return std::sqrt(a * a * std::cos(2 * theta));
        }
    }

    std::string toString() {
        std::stringstream ss;
        ss << "r^2 = (" << c << ")^2 ";
        if (type == SIN) {
            ss << "sin";
        } else {
            ss << "cos";
        }
        ss << "(2 * theta)";

        return ss.str();
    }
};

int main() {
    /////////// Window and graph setup ////////////

    //Set-up the window
    sf::VideoMode videoMode(SCREEN_WIDTH, SCREEN_HEIGHT);
    sf::RenderWindow renderWindow(videoMode, "Animated Polar Plotting");

    //Initialize the input states
    keyStates.fill(false);

    //Initialize the graph axes array
    sf::VertexArray axes(sf::PrimitiveType::Lines, 4);
    axes[0] =
            (sf::Vertex(sf::Vector2f(0, SCREEN_HEIGHT / 2), sf::Color::Black));
    axes[1] = (sf::Vertex(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT / 2),
            sf::Color::Black));
    axes[2] = (sf::Vertex(sf::Vector2f(SCREEN_WIDTH / 2, 0), sf::Color::Black));
    axes[3] = (sf::Vertex(sf::Vector2f(SCREEN_WIDTH / 2, SCREEN_HEIGHT),
            sf::Color::Black));

    //Initialize curves
    float numCycles = 1;
    std::vector<PolarCurve*> curves(4);
    curves[LIMACON] = new Limacon(QUALITY, numCycles, sf::Color::Black);
    curves[CARDIOD] = new Cardiod(QUALITY, numCycles, sf::Color::Black);
    curves[ROSE_CURVE] = new RoseCurve(QUALITY, numCycles, sf::Color::Black);
    curves[LEMNISCATE] = new Lemniscate(QUALITY, numCycles, sf::Color::Black);
    int selected = LIMACON;

    //Initialize text and font
    sf::Font font;
    font.loadFromFile("Roboto-Black.ttf");
    sf::Text info;
    info.setFont(font);
    info.setColor(sf::Color::Black);
    info.setCharacterSize(36);
    info.setPosition(10, 0);
    sf::Text help;
    help.setFont(font);
    help.setColor(sf::Color::Black);
    help.setCharacterSize(25);
    help.setPosition(10, SCREEN_HEIGHT - 30);
    help.setString(
            "(1/2/3/4) Select Curve, (Q/A) Change a, (W/S) Change b, (E/D) Change theta range, (Space) Change sin/cos, (R) Reset");

    //Initialize timer
    sf::Clock clock;

    /////////// Main program loop ////////////
    while (renderWindow.isOpen()) {
        float deltaTime = clock.restart().asSeconds();

        /////////// Handle input events ////////////
        sf::Event event;
        while (renderWindow.pollEvent(event)) {
            switch (event.type) {
                case (sf::Event::Closed):
                    renderWindow.close();
                    break;
                case (sf::Event::KeyPressed):
                    keyStates[static_cast<int>(event.key.code)] = true;
                    switch (event.key.code) {
                        case sf::Keyboard::Escape:
                            renderWindow.close();
                            break;
                        case sf::Keyboard::Num1:
                            selected = LIMACON;
                            break;
                        case sf::Keyboard::Num2:
                            selected = CARDIOD;
                            break;
                        case sf::Keyboard::Num3:
                            selected = ROSE_CURVE;
                            break;
                        case sf::Keyboard::Num4:
                            selected = LEMNISCATE;
                            break;
                        case sf::Keyboard::Space:
                            curves[selected]->switchType();
                            break;
                        default:
                            break;
                    }
                    break;
                case (sf::Event::KeyReleased):
                    keyStates[static_cast<int>(event.key.code)] = false;
                    break;
                default:
                    break;
            }
        }

        //Increase/Decrease a
        if (isKeyPressed(sf::Keyboard::Q)) {
            curves[selected]->a += SPEED * deltaTime;
        }
        if (isKeyPressed(sf::Keyboard::A)) {
            curves[selected]->a -= SPEED * deltaTime;
        }
        //Increase/Decrease b
        if (isKeyPressed(sf::Keyboard::W)) {
            curves[selected]->b += SPEED * deltaTime;
        }
        if (isKeyPressed(sf::Keyboard::S)) {
            curves[selected]->b -= SPEED * deltaTime;
        }
        //Prevent a and b from being 0
        if (curves[selected]->a == 0) {
            curves[selected]->a = 1;
        }
        if (curves[selected]->b == 0) {
            curves[selected]->b = 1;
        }
        //Increase/Decrease theta range
        if (isKeyPressed(sf::Keyboard::E)) {
            numCycles += SPEED * deltaTime;
        }
        if (isKeyPressed(sf::Keyboard::D)) {
            numCycles = std::fmax(0, numCycles - SPEED * deltaTime);
        }
        //Reset
        if(isKeyPressed(sf::Keyboard::R)) {
            numCycles = 1;
            for(PolarCurve *p : curves) {
                p->a = 1;
                p->b = 1;
                p->type = SIN;
            }
        }

        /////////// Update window display ////////////
        renderWindow.clear(sf::Color::White);

        //Draw graph axes
        renderWindow.draw(axes);

        //Update the selected curve
        for (int i = 0; i < curves.size(); i++) {
            if (i == selected) {
                curves[i]->updateNumCycles(numCycles);
                curves[i]->update();
                renderWindow.draw(*curves[i]);
            }
        }

        //Update and draw equation string display
        std::string display = "";
        display.append("--- ").append(NAMES[selected]).append(" ---\n");
        display.append(curves[selected]->toString());
        display.append("\n0 <= theta <= ");
        display.append(std::to_string(2 * numCycles)).append("pi");
        info.setString(display);
        renderWindow.draw(info);

        //Update and draw controls help
        renderWindow.draw(help);

        renderWindow.display();
    }
}
