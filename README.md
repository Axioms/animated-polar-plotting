# Animated-Polar-Plotting
Visually demonstrating the plotting of several types of polar curves.
[View Source Code](https://bitbucket.org/Axioms/animated-polar-plotting/src)

## Example
![Example](http://i.imgur.com/DZOOnZ4.gif)

[(Direct Link (.gif))](http://i.imgur.com/DZOOnZ4.gifv)

## Project Reflection
The polar coordinate system is a useful method of defining points on a plane, but can sometimes produce unintuitive curves and graphs that are difficult to grasp when coming from a cartesian mindset. This program was created with the intent of understanding the types of polar curves, looking at how the coefficients in each affect their appearances and what parts of them correspond to particular ranges of theta values.

The four curves experimented with in this program are:  
1. **Limacon** `r = a ± b * cos(θ), r = a ± b * sin(θ)` Where a ≠ 0 and b ≠ 0  
2. **Cardiod** `r = a ± a * cos(t), r = a ± a * sin(t)`. As can be seen, cardiods are a special case of Limacons where `a` and `b` are equal.  
3. **Rose Curve** `r = a * sin(b*θ), r = a * sin(b*θ)` Where a ≠ 0 and b ≠ 0  
4. **Lemniscate** `r^2 = a^2 * sin(2θ), r^2 = a^2 * cos(2θ)` Where a ≠ 0  

To translate each of these polar equations into cartesian ones (as is needed to drawn as pixels on the screen), the following equalities can be used:  
Translating polar equations into cartesian ones can be done using the following equalities:  
**x = r * cos(θ)**  
**y = r * sin(θ)**  
**y/x = tan(θ)**  
**x^2 + y^2 = r^2**  
The first three can simply be derived from trigonometric definitions for sin, cos, and tan, while the last is the equation of a circle. 

Looking at more examples of these curves being drawn below, the progressive development of each can be seen when the range of θ is changed to be smaller.

**Limacon**
![Limacon](http://imgur.com/pcdfVmw.gif)
[(Direct Link (.gif))](http://imgur.com/pcdfVmw.gifv)

**Cardiod**
![Cardiod](http://imgur.com/734cfs7.gif)
[(Direct Link (.gif))](http://imgur.com/734cfs7.gifv)

**Lemniscate**
![Lemniscate](http://imgur.com/sVsnIKQ.gif)
[(Direct Link (.gif))](http://imgur.com/sVsnIKQ.gifv)


## Additional Details
This program was created using the [SFML](http://www.sfml-dev.org/) graphics library.